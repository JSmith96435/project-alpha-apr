from django.shortcuts import render
from projects.models import Projects

# Create your views here.


def list_projects(request):
    projects = Projects.objects.filter(purchaser=request.user)
    context = {
        "projects": projects,
    }
    return render(request, "projects/list.html", context)
