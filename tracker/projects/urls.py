from django.urls import path
from projects.views import (
    list_projects,
    create_project,
)


urlpatterns = [
    path("", list_projects, name="home"),
    path("<int:id>/", name="show_project"),
]
